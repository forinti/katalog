@extends('layouts.app')


@section('content')
    <div class="row">
        <form class="form-inline col-md" action="{{route('search')}}" method="GET">
           {{csrf_field()}}
            <input class="form-control col-md-11" required name = "query" type="search" placeholder="Поиск" aria-label="Поиск">
            <div class="col-md-1">
                <button class="btn btn-outline-success" type="submit">Найти</button>
            </div>
        </form>
    </div>
    <br>
    <div class="card">
        <div class="card-header">
            20 популярных товаров
        </div>
        <div class="card-body">

            @foreach($products as $key => $product)
                <div class="card w-100" style="margin-bottom: 10px;">
                    <div class="card-body">
                        <h5 class="card-title">{{$product->title}}</h5>
                        <p class="card-text">{{$product->description}}</p>
                        <img src="{{$product->image}}" alt="">
                    </div>
                </div>
            @endforeach
        </div>
    </div>

@endsection