@extends('layouts.app')
@section('content')
    <form class="form-inline col-md" action="{{route('search')}}" style = "margin-bottom: 10px" method="GET">
        {{csrf_field()}}
        <input class="form-control col-md-11" required name = "query" type="search" placeholder="Поиск" aria-label="Поиск">
        <div class="col-md-1">
            <button class="btn btn-outline-success" type="submit">Найти</button>
        </div>
    </form>
    <div class="card">
        <div class="card-header">
            Результаты поиска по запросу {{$searchQuery}}
        </div>
        <div class="card-body">
            @if (count($products)>0)
                @foreach($products as $key => $product)
                    <div class="card w-100" style="margin-bottom: 10px;">
                        <div class="card-body">
                            <h5 class="card-title">{{$product->title}}</h5>
                            <p class="card-text">{{$product->description}}</p>
                            <img src="{{$product->image}}" alt="">
                        </div>
                    </div>
                @endforeach
            @else
                <div class="alert alert-warning">
                    Ничего не найдено
                </div>
            @endif
        </div>

        <div class = "row align-items-center justify-content-center">
            <div class="row">
                {{$products->links()}}
            </div>
        </div>
    </div>

@endsection