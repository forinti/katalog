@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            Товары категории "{{$category->title}}"
        </div>
        <div class="card-body">

            @foreach($products as $key => $product)
                <div class="card w-100" style="margin-bottom: 10px;">
                    <div class="card-body">
                        <h5 class="card-title">{{$product->title}}</h5>
                        <p class="card-text">{{$product->description}}</p>
                        <img src="{{$product->image}}" alt="">
                    </div>
                </div>
            @endforeach
        </div>
        <div class = "row align-items-center justify-content-center">
            <div class="row">
        {{$products->links()}}
            </div>
        </div>
    </div>

@endsection