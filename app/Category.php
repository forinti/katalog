<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $fillable = ['id', 'title', 'alias', 'parent'];
    protected $guarded = [];


    public function products()
    {
        return $this->belongsToMany('App\Product', 'product_category', 'category_id', 'product_id');
    }

    public function children()
    {
        return $this->hasMany('App\Category', 'parent', 'id');
    }

    public function parent()
    {
        return $this->hasOne('App\Category', 'id', 'parent');
    }

    public function hasChildren()
    {

        return count($this->children) > 0;
    }

}
