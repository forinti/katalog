<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Category;
use App\Offer;
use App\Product;
use Illuminate\Support\Facades\Log;

class ExportDB extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:db';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $url = "https://markethot.ru/export/bestsp";
        $response = json_decode(file_get_contents($url));
        foreach ($response->products as $product) {
            foreach ($product->categories as $category) {
                unset($category->acrm_id);
                $c = Category::updateOrCreate(['id' => $category->id], (array)$category);
            }
            foreach ($product->offers as $offer) {
                unset($offer->acrm_id);//удаляю т.к. в задании ничего не сказано про это поле
                $o = Offer::updateOrCreate(['id' => $offer->id], (array)$offer);
            }
            $offersId = array_column((array)$product->offers, 'id');
            $categoriesId = array_column((array)$product->categories, 'id');

            unset($product->last_supplied);
            unset($product->category_google); // также как и last_supplied не упомянуто в тексте задания
            unset($product->categories);
            unset($product->offers);

            $p = Product::updateOrCreate(['id' => $product->id], (array)$product);
            if (!is_null($offersId))
                $p->offers()->attach($offersId);
            if (!is_null($categoriesId))
                $p->categories()->attach($categoriesId);

        }
    echo("Export succesful\n");
    }
}
