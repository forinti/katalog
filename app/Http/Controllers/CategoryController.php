<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;

class CategoryController extends Controller
{
    public function show($alias){
        $currentCategory = Category::where('alias',$alias)->first();
        
        $products = $currentCategory->products()->paginate(10);

        $rootCategories = Category::where('parent',null)->with('children')->get();


        return view('catalog.category',['products'=>$products,'categories'=>$rootCategories,'category'=>$currentCategory]);
    }
}
