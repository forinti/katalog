<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Offer;
use App\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class IndexController extends Controller
{
    public function index() {
        $mostPopularProducts = DB::table('products')->select(DB::raw('products.*,SUM(offers.sales) as total_sales'))
            ->join('product_offer','products.id','=','product_offer.product_id')
            ->join('offers','offers.id','=','product_offer.offer_id')
            ->orderBy('total_sales','desc')
            ->groupBy('products.id')
            ->limit(20)
            ->get();

        $rootCategories = Category::where('parent',null)->with('children')->get();

        return view('catalog.index',['products'=>$mostPopularProducts,'categories'=>$rootCategories,]);
    }

    public function search(Request $request){

        $rootCategories = Category::where('parent',null)->with('children')->get();
        $searchQuery = $request->get('query');
        if ($searchQuery != null) {
            Session::put('searchQuery', $searchQuery);
        } else {
            $searchQuery = Session::get('searchQuery');
        }
        $products = Product::where('title','like',"%$searchQuery%")->orWhere('description','like',"%$searchQuery%")->paginate(10);
        return view('catalog.search',['categories'=>$rootCategories,'products'=>$products, 'searchQuery'=>$searchQuery]);
    }
}
