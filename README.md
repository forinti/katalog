# Каталог
nginx config
```
server {
   listen 80;
   listen [::]:80;

   root /path/to/your/project/public;
   index index.php index.html index.htm;

   server_name catalog;

   location / {
       try_files $uri $uri/ /index.php$is_args$args;
   }

   location ~ \.php$ {
       try_files $uri /index.php =404;

       fastcgi_split_path_info ^(.+\.php)(/.+)$;
       fastcgi_pass unix:/run/php/php7.1-fpm.sock;
       fastcgi_index index.php;
       fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
       include /etc/nginx/fastcgi_params;
   }
}
```
#
cd /path/to/your/project/
1. composer install
2. npm install 
3. php artisan migrate
4. php artisan export:db
5. crontab -e, далее вставить эту строку: * * * * * php /path-to-your-project/artisan schedule:run >> /dev/null 2>&1
6. php artisan schedule:run
7. php artisan serve 
#
