<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','IndexController@index');
Route::get('search','IndexController@search')->name('search');
Route::get('categories/{slug}','CategoryController@show')->name('category');
Route::get('/export', 'CategoryController@export');
